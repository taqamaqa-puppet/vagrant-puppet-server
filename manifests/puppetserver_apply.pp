# /!\ CAUTION
#
# SSH public key of user running r10k (typically root) *MUST* have been declared in
# https://gitlab.com/taqamaqa-puppet/control-repo/-/settings/repository#js-deploy-keys-settings
# *BEFORE* applying this code.

class { 'r10k':
  remote => 'git@gitlab.com:taqamaqa-puppet/control-repo.git',
}