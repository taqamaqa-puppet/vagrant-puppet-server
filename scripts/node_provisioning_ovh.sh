#!/bin/bash

# #############################################################################
# Variables
# #############################################################################

PUPPET_PACKAGE="puppet6-release-bionic.deb"
PUPPET_AGENT_CONFIG_FILE="/etc/puppetlabs/puppet/puppet.conf"
PUPPET="/opt/puppetlabs/bin/puppet"

MY_PUPPET_SERVER="puppetserver.taqamaqa.com"


# #############################################################################
# functions
# #############################################################################

# Install Puppet agent package
function puppet_agent_install() {
  if [ ! -f "${PUPPET}" ]; then
    sudo wget --quiet "https://apt.puppetlabs.com/${PUPPET_PACKAGE}"
    sudo dpkg -i "${PUPPET_PACKAGE}"
    sudo apt-get update
    sudo apt-get install puppet-agent
  fi
}

# Configure Puppet agent
function puppet_agent_config() {
  # Create config file if necessary
  if [ ! -f "${PUPPET_AGENT_CONFIG_FILE}" ]; then
    touch "${PUPPET_AGENT_CONFIG_FILE}"
  fi

  "${PUPPET}" config set server "${MY_PUPPET_SERVER}"
  "${PUPPET}" config set certname "$(/bin/hostname -f)"
}

# Register node to puppetserver
function register_node() {
  "${PUPPET}" agent --test
}

# #############################################################################
# main
# #############################################################################
puppet_agent_install
puppet_agent_config
register_node