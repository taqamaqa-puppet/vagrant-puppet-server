# vagrant-puppet-server

Vagrantbox to test deployment of Puppet Server

## Prerequisites

* Virtualbox 
* Vagrant
* git
* r10k (see below how to do this properly)

## r10k installation on a workstation

TODO: this should be correctly explained and tested (which by now may not be the case :) )

1. On your workstation, install [RBenv](https://github.com/rbenv/rbenv). RBenv allows a user to have different versions of Ruby running on a system and isolate them. This is a great help when using Puppet on Linux, as Ruby versions shipped with distros can be quite old. Moreover, RBenv allows user to have a Ruby installation in its user-space, so that no root privileges are required.
1. With RBenv install Ruby 2.5.x (please follow RBenv documentation to do that)
1. Install r10k: `gem install r10k`

## Usage

Grab this repository, get into it, grab Puppet modules needed and then run Vagrant:

```
git clone https://gitlab.com/taqamaqa-puppet/vagrant-puppet-server.git
cd vagrant-puppet-server
./BUILD.sh
vagrant up
```

## What is done

The vagrantfile describes two virtual machines (VMs) : `puppetserver.example.org` and `node.example.org`. They are both placed on a private network (`10.0.0.0/24`)

File `manifests/default_puppetserver.pp` is the Puppet code that installs Puppet Server. 

The `BUILD.sh` script downloads Puppet modules that are required for our Puppet code (`default_puppetserver.pp` and `default_node.pp`). This is done with the utility `r10k`. Dependencies are listed in the `Puppetfile`, which is read by `r10k`.

Each Puppet module has a decription page on the [Puppet Forge](https://forge.puppet.com/). For instance, we use module [TheForeman / Puppet](https://forge.puppet.com/theforeman/puppet/readme) to install Puppet server.

## What is done on Puppetserver.example.org

Puppet package is installed with shell (inline) provisionning. All other operations are done by Puppet provisioning. Puppet server is installed and configured with module `theforeman/puppet` (The Foreman is *not* installed).

To let R10k grab the control repo, a few things have to be done beforehand:

* the gitlab.com public key is added
* the SSH keys for user `root` are created

Then the SSH public key of user `root` must be *MANUALLY* added to 
https://gitlab.com/taqamaqa-puppet/control-repo/-/settings/repository#js-deploy-keys-settings

Therefore, in order to actually install R10k and our control repo (`git@gitlab.com:taqamaqa-puppet/control-repo.git`), the following command must be issued as `root` into the Puppetserver host:

```shell script
/opt/puppetlabs/bin/puppet apply --modulepath=/vagrant/modules/ /vagrant/manifests/puppetserver_apply.pp
```

**Note**: The R10k module only installs r10k, it does *not* download the control repo. This can be done manually with
`r10k deploy environment --puppetfile --verbose` or programmatically with a webhook (cf https://forge.puppet.com/puppet/r10k#webhook-support)

## TODO

1. Think about how to transpose this to a fresh new Public Cloud instance
    * Installing Puppet package is easy
    * For the very first run, modules may have to installed with `puppet module install` (as r10k will be installed afterwards)