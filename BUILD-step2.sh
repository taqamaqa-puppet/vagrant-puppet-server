#!/bin/bash

# This script is meant to be run after BUILD.sh *and* its manual actions

# #############################################################################
# Variables
# #############################################################################

PUPPET="/opt/puppetlabs/bin/puppet"
PUPPETSERVER="/opt/puppetlabs/bin/puppetserver"

# #############################################################################
# functions
# #############################################################################
function setup_r10k_control_repo() {
  # Install R10k
  vagrant ssh puppetserver -c \
    "sudo ${PUPPET} apply --modulepath=/vagrant/modules/ /vagrant/manifests/puppetserver_apply.pp"

  # Grab control repo
  vagrant ssh puppetserver -c 'sudo /usr/bin/r10k deploy environment --puppetfile --verbose'
}

# Launch VM, register to puppet server, run puppet agent
function launch_node_vm() {
  VM_NAME="$1"
  VM_FQDN="$2"

  # Launch node VM
  vagrant up "${VM_NAME}"

  # Node certificate validation
  vagrant ssh "${VM_NAME}" -c "sudo ${PUPPET} agent -t"
  vagrant ssh puppetserver -c "sudo ${PUPPETSERVER} ca sign --certname ${VM_FQDN}"

  # First real puppet run
  vagrant ssh "${VM_NAME}" -c "sudo ${PUPPET} agent -t"
}

# #############################################################################
# main
# #############################################################################
setup_r10k_control_repo
launch_node_vm node node.example.org
launch_node_vm prometheus prometheus.example.org
launch_node_vm grafana grafana.example.org
