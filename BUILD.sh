#!/bin/bash

r10k puppetfile install -v
vagrant up puppetserver

# #############################################################################
# MANUAL STEPS

# #############################################################################
# Step 1:
# add puppetserver's root user public key to
# https://gitlab.com/taqamaqa-puppet/control-repo/-/settings/repository#js-deploy-keys-settings


