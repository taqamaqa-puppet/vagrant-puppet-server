$script_bionic = <<-SCRIPT
if [ ! -f /opt/puppetlabs/bin/puppet ]; then
 
  sudo wget --quiet https://apt.puppetlabs.com/puppet6-release-bionic.deb
  sudo dpkg -i puppet6-release-bionic.deb
  sudo apt-get update
  sudo apt-get install puppet-agent
fi
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.define "puppetserver" do |pp|
    pp.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.cpus = "2"
    end
    pp.vm.box = "ubuntu/bionic64"
    pp.vm.network "private_network", ip: "10.0.0.10"
    pp.vm.hostname = "puppetserver.example.org"
    pp.vm.provision "shell", inline: $script_bionic
    pp.vm.provision "puppet" do |pp1|
      pp1.module_path = "modules"
      pp1.manifest_file = "default_puppetserver.pp"
    end
  end

  config.vm.define "node" do |n|
    n.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = "1"
    end
    n.vm.box = "ubuntu/bionic64"
    n.vm.network "private_network", ip: "10.0.0.11"
    n.vm.network "forwarded_port", guest: 9100, host: 9111
    n.vm.hostname = "node.example.org"
    n.vm.provision "shell" do |n1|
        n1.path = "scripts/node_provisioning.sh"
        n1.args = "node.example.org"
    end
  end

  config.vm.define "prometheus" do |pr|
    pr.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = "1"
    end
    pr.vm.box = "ubuntu/bionic64"
    pr.vm.network "private_network", ip: "10.0.0.12"
    pr.vm.network "forwarded_port", guest: 9090, host: 9090
    pr.vm.network "forwarded_port", guest: 9100, host: 9112
    pr.vm.network "forwarded_port", guest: 9115, host: 9115
    pr.vm.hostname = "prometheus.example.org"
    pr.vm.provision "shell" do |pr1|
        pr1.path = "scripts/node_provisioning.sh"
        pr1.args = "prometheus.example.org"
    end
  end

  config.vm.define "grafana" do |g|
    g.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.cpus = "2"
    end
    g.vm.box = "ubuntu/bionic64"
    g.vm.network "private_network", ip: "10.0.0.13"
    g.vm.network "forwarded_port", guest: 3000, host: 3000
    g.vm.hostname = "grafana.example.org"
    g.vm.provision "shell" do |g1|
        g1.path = "scripts/node_provisioning.sh"
        g1.args = "grafana.example.org"
    end
  end
end

